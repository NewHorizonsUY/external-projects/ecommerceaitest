import { defineConfig } from 'astro/config';
import tailwind from "@astrojs/tailwind";

import compress from "astro-compress";

// https://astro.build/config
export default defineConfig({
  outDir: 'public',
  publicDir: 'static',
  site: 'https://ecommerceaitest.newhorizons.uy',
  integrations: [tailwind(), compress()]
});
